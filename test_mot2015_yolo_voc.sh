#!/bin/bash
#OAR -p gpu='YES' and cluster='dellt630gpu'
#OAR -l/gpunum=1,walltime=2
#OAR --notify mail:juan-diego.gonzales-zuniga@inria.fr

module load cuda/8.0 cudnn/6.0-cuda-8.0
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64"
make -j8
./darknet caltech test_list cfg/mot-ADL-Rundle-1.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-ADL-Rundle-3.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-AVG-TownCentre.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-ETH-Crossing.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-ETH-Jelmoli.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-ETH-Linthescher.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-KITTI-16.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-KITTI-19.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-PETS09-S2L2.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-TUD-Crossing.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/mot-Venice-1.data cfg/yolo-voc.cfg weights/yolo-voc.weights



