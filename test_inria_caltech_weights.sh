#!/bin/bash
#OAR -p gpu='YES' and cluster='dellt630gpu'
#OAR -l/gpunum=1,walltime=1
#OAR --notify mail:juan-diego.gonzales-zuniga@inria.fr

module load cuda/8.0 cudnn/6.0-cuda-8.0
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64"
make -j8
./darknet caltech test cfg/caltech.data cfg/yolo-voc.cfg weights/yolo-caltech.weights -dataset data-INRIA -set 01 -thresh 0
