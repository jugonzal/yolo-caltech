#include "darknet.h"
#include <dirent.h>
void test_caltech(char *datacfg, char *cfgfile, char *weightfile, char *set, float thresh, float hier_thresh, int fullscreen, char *dataset)
{
    /* NETWORK CONFIGURATION */
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", "data/names.list");
    char **names = get_labels(name_list);
    
    image **alphabet = load_alphabet();
    network net = parse_network_cfg(cfgfile);
    if(weightfile){
        load_weights(&net, weightfile);
    }
    set_batch_network(&net, 1);
    srand(22222222);
    
    double time;
    char buff[256];
    char *input = buff;
    float nms=.3;
    
    /* File configuration */    

    char **files;
    char **dirs;
    size_t count_files;
    size_t count_dirs;
    int i;
    int j;
    int k;
    
    char *path_set;
    char *V0txt;
    char *outfile;
    printf("%s\n", dataset); 
    path_set = concat_path_set(set, dataset);
    count_dirs = file_list(path_set, &dirs, DT_DIR);

    for ( i = 0; i< count_dirs; i++) {
        char *path_V0;
        char *path_V0_res;
        path_V0 = concat_path(path_set, dirs[i]);
        count_files = file_list(path_V0, &files, DT_REG);
        
        path_V0_res = concat_path_res(set,dirs[i], dataset);
        V0txt = concat_txt(path_V0_res);

        //printf("%s\n", V0txt);

	for (j=0; j < count_files; j++) {
             char *path_image;
             path_image = concat_path(path_V0,files[j]);
             outfile = concat_path(path_V0_res, files[j]);

             //printf("%s\n", path_image);
             
             if(path_image) {
		 strncpy(input,path_image, 256);
	     } else {
                 printf("Enter Image Path: ");
                 fflush(stdout);
                 input = fgets(input, 256, stdin);
                 if(!input) return;
                 strtok(input, "\n");
	     }
	     image im = load_image_color(input,0,0);
             image sized = letterbox_image(im, net.w, net.h);
             layer l = net.layers[net.n-1];
       
	     box *boxes = calloc(l.w*l.h*l.n, sizeof(box));
	     float **probs = calloc(l.w*l.h*l.n, sizeof(float *));
             for (k = 0; k < l.w*l.h*l.n; ++k) probs[k] = calloc(l.classes + 1, sizeof(float *));
	     float **masks = 0;
	     if (l.coords > 4) {
	   	masks = calloc(l.w*l.h*l.n, sizeof(float *));
	        for(k = 0; k < l.w*l.h*l.n; ++k) masks[k] = calloc(l.coords - 4, sizeof(float *));
	     }
	
	     float *X = sized.data;
 	     time = what_time_is_it_now();
	     network_predict(net, X);
	     printf("%s: Predicted in %f seconds.\n", input, what_time_is_it_now()-time);
	     get_region_boxes(l, im.w, im.h, net.w, net.h, thresh, probs, boxes, masks, 0, 0, hier_thresh, 1);
	
	     if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
             draw_detections_caltech(im, l.w*l.h*l.n, thresh, boxes, probs, masks, names, alphabet, l.classes, j +1, V0txt);
             
             if(outfile){
                save_image(im, outfile);
             }
             else{
                save_image(im, "predictions");
             #ifdef OPENCV
                cvNamedWindow("predictions", CV_WINDOW_NORMAL);
                if(fullscreen){
                    cvSetWindowProperty("predictions", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
                }
                show_image(im, "predictions");
                cvWaitKey(0);
                cvDestroyAllWindows();
             #endif
             }
             
             free_image(im);
             free_image(sized);
	     free(boxes);
	     free_ptrs((void **)probs, l.w*l.h*l.n);
	     //if (path_image) break;
             free(path_image);
             free(outfile);
            
        }
        free(path_V0);
    }
}

void test_list(char *datacfg, char *cfgfile, char *weightfile, float thresh, float hier_thresh, int fullscreen)
{
    /* NETWORK CONFIGURATION */
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", "data/names.list");
    char **names = get_labels(name_list);
    char *test_images = option_find_str(options, "test", "data/test.list");
    list *tlist = get_paths(test_images);
    char **paths = (char **)list_to_array(tlist);
    char *res_test = option_find_str(options, "res", "data/res.txt");

    image **alphabet = load_alphabet();
    network net = parse_network_cfg(cfgfile);
    if(weightfile){
        load_weights(&net, weightfile);
    }
    set_batch_network(&net, 1);
    srand(22222222);
    
    double time;
    char buff[256];
    char *input = buff;
    float nms=.3;
    
    /* File configuration */    
    int N = tlist->size;
    int i;
    int k;
    char *V0txt = res_test;
    char *outfile;  
    for ( i = 0; i< N; i++) {
        char * path_image;
        path_image = paths[i];
        outfile = strdup(path_image); 
        if(path_image) {
	   strncpy(input,path_image, 256);
        } else {
           printf("Enter Image Path: ");
           fflush(stdout);
           input = fgets(input, 256, stdin);
           if(!input) return;
              strtok(input, "\n");
        }
        image im = load_image_color(input,0,0);
        image sized = letterbox_image(im, net.w, net.h);
        layer l = net.layers[net.n-1];
    
        box *boxes = calloc(l.w*l.h*l.n, sizeof(box));
        float **probs = calloc(l.w*l.h*l.n, sizeof(float *));
        for (k = 0; k < l.w*l.h*l.n; ++k) probs[k] = calloc(l.classes + 1, sizeof(float *));
        float **masks = 0;
        if (l.coords > 4) {
  	    masks = calloc(l.w*l.h*l.n, sizeof(float *));
	    for(k = 0; k < l.w*l.h*l.n; ++k) masks[k] = calloc(l.coords - 4, sizeof(float *));
	}
	
	float *X = sized.data;
 	time = what_time_is_it_now();
	network_predict(net, X);
	printf("%s: Predicted in %f seconds.\n", input, what_time_is_it_now()-time);
	get_region_boxes(l, im.w, im.h, net.w, net.h, thresh, probs, boxes, masks, 0, 0, hier_thresh, 1);
	
	if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
            draw_detections_caltech(im, l.w*l.h*l.n, thresh, boxes, probs, masks, names, alphabet, l.classes, i +1, V0txt);
        if(outfile){
                save_image(im, outfile);
        }
     
        free_image(im);
        free_image(sized);
        free(boxes);
	free_ptrs((void **)probs, l.w*l.h*l.n);
        free(path_image);
        free(outfile);
            
    }
    
}

void run_caltech(int argc, char **argv)
{
    float thresh = find_float_arg(argc, argv, "-thresh", .24);
    float hier_thresh = find_float_arg(argc, argv, "-hier", .5);
    //char *outfile = find_char_arg(argc, argv, "-out", 0);
    char *set = find_char_arg(argc, argv, "-set", "00");
    char *dataset = find_char_arg(argc, argv, "-dataset", "data-USA");
    //char *V0 = find_char_arg(argc, argv, "-V0", "00");
    int fullscreen = find_arg(argc, argv, "-fullscreen");
    char *datacfg = argv[3];
    char *cfg = argv[4];
    char *weights = (argc > 5) ? argv[5] : 0;
    if(argc < 4){
        fprintf(stderr, "usage: %s %s [test] [cfg] [weights (optional)]\n", argv[0], argv[1]);
        return;
    }
    if(0==strcmp(argv[2], "test")) test_caltech(datacfg, cfg, weights, set, thresh,  hier_thresh, fullscreen, dataset);
    else if(0==strcmp(argv[2], "test_list")) test_list(datacfg, cfg, weights, thresh, hier_thresh, fullscreen);

}


