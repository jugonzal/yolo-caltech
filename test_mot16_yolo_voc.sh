#!/bin/bash
#OAR -p gpu='YES' and cluster='dellt630gpu'
#OAR -l/gpunum=1,walltime=2
#OAR --notify mail:juan-diego.gonzales-zuniga@inria.fr

module load cuda/8.0 cudnn/6.0-cuda-8.0
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64"
make -j8
./darknet caltech test_list cfg/MOT16-01.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/MOT16-03.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/MOT16-06.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/MOT16-07.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/MOT16-08.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/MOT16-12.data cfg/yolo-voc.cfg weights/yolo-voc.weights
./darknet caltech test_list cfg/MOT16-14.data cfg/yolo-voc.cfg weights/yolo-voc.weights



