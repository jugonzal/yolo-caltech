#ifndef CALTECH_DATA_H
#define CALTECH_DATA_H

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <malloc.h>
#include <limits.h>
#include <sys/stat.h>
#include <errno.h>

#define caltechImages "./data/data-USA/images"
#define caltechAnnotations "./data/data-USA/annotations"
#define caltechResults "./results/data-USA/yolo"

#define inriaImages "./data/data-INRIA/images"
#define inriaAnnotations "./data/data-INRIA/annotations"
#define inriaResults "./results/data-INRIA/yolo"


size_t file_list(const char *path, char ***ls, unsigned char file_type);
char *concat_path_set (char *set, char  *dataset);
char *concat_path (char *path, char *file);
char *concat_txt (char *path);
char *concat_path_res (char *set, char *V0, char *dataset );

#endif 
