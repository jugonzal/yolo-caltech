#include "caltech_data.h"

int cstring_cmp(const void *a, const void *b)
{
   return strcmp(*(char **)a, *(char **)b);
}

int mkdir_p(const char *path)
{
    /* Adapted from http://stackoverflow.com/a/2336245/119527 */
    const size_t len = strlen(path);
    char _path[PATH_MAX];
    char *p; 

    errno = 0;

    /* Copy string so its mutable */
    if (len > sizeof(_path)-1) {
        errno = ENAMETOOLONG;
        return -1; 
    }   
    strcpy(_path, path);

    /* Iterate the string */
    for (p = _path + 1; *p; p++) {
        if (*p == '/') {
            /* Temporarily truncate */
            *p = '\0';

            if (mkdir(_path, S_IRWXU) != 0) {
                if (errno != EEXIST)
                    return -1; 
            }

            *p = '/';
        }
    }   

    if (mkdir(_path, S_IRWXU) != 0) {
        if (errno != EEXIST)
            return -1; 
    }   

    return 0;
}


size_t file_list(const char *path, char ***ls, unsigned char file_type) {
    size_t count = 0;
    DIR *dp = NULL;
    struct dirent *ep = NULL;

    dp = opendir(path);
    if(NULL == dp) {
        fprintf(stderr, "no such directory: '%s'\n", path);
        return 0;
    }

    *ls = NULL;
    ep = readdir(dp);
    while(NULL != ep){

            if(ep->d_type==file_type)
                count++;
        ep = readdir(dp);
    }

    rewinddir(dp);
    *ls = calloc(count, sizeof(char *));
    
    count = 0;
    ep = readdir(dp);
    while(NULL != ep){
            if(ep->d_type==file_type)
            (*ls)[count++] = strdup(ep->d_name);
        ep = readdir(dp);
    }
    qsort((*ls), count, sizeof(char *), cstring_cmp);
    closedir(dp);
    return count;
}

char *concat_path_set (char *set, char *dataset) {
    
    char * PathImages;
    PathImages = NULL;
    if ((strcmp(dataset, "data-USA") != 0) && (strcmp(dataset, "data-INRIA") != 0) )  printf("DATASET not accepted %s\n", dataset);
    if (strcmp(dataset, "data-USA") == 0 ) PathImages= strdup(caltechImages);
    if (strcmp(dataset, "data-INRIA") == 0 ) PathImages= strdup(inriaImages);
    size_t pathlen = strlen(PathImages)+ strlen("/set") + strlen(set)+1;
    char *path = malloc(pathlen);
    strcpy(path, PathImages);
    strcat(path,"/set");
    strcat(path,set);
    return path;

}

char *concat_path_res (char *set, char *V0, char *dataset ) {

    char *PathResults;
    PathResults = NULL;
    if ((strcmp(dataset, "data-USA") != 0) && (strcmp(dataset, "data-INRIA") != 0) )  printf("DATASET not accepted %s\n", dataset);
    if (strcmp(dataset, "data-USA") == 0 ) PathResults =strdup(caltechResults);
    if (strcmp(dataset, "data-INRIA") == 0 ) PathResults= strdup(inriaResults);

    size_t pathlen = strlen(PathResults)+ strlen("/set") + strlen(set)+ strlen("/") + strlen(V0)  + 1;
    char *path = malloc(pathlen);
    strcpy(path, PathResults);
    strcat(path,"/set");
    strcat(path,set);
    strcat(path, "/");
    strcat(path,V0);
    const char * creation = path;
    mkdir_p(creation);    
    return path;

}

char *concat_path (char *path, char *file) {
    size_t pathlen = strlen(path) + strlen(file) + 2;
    char *fullpath= malloc(pathlen);
    sprintf(fullpath, "%s/%s", path, file);
    return fullpath;

}

char *concat_txt (char *path) {
    size_t pathtxtlen = strlen(path) + 3 + 2;
    char *fullpath= malloc(pathtxtlen);
    sprintf(fullpath, "%s.txt", path);
    return fullpath;
}

/*
int main(int argc, char **argv) {

    char **files;
    char **dirs;
    size_t count_files;
    size_t count_dir;
    int i;
    int j;
    char *set = "10";
    char *dataset = "data-USA";
    char *path_set;

    FILE *f = fopen("./data/data-USA/test.txt", "a");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    
    path_set = concat_path_set(set, dataset);
    count_dir = file_list(path_set, &dirs, DT_DIR);

    for (j = 0; j < count_dir; j++) {
        char *path_V0;
	path_V0 = concat_path(path_set, dirs[j]);
        count_files = file_list(path_V0, &files, DT_REG);
        for (i = 0; i < count_files; i++) {
	    char *path_image;
            path_image = concat_path(path_V0, files[i]);
            fprintf(f, "%s\n", path_image);
            free(path_image);
	}
        free(path_V0);
    }
    
}*/
