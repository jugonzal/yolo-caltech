import xml.etree.ElementTree as ET
import pickle
from PIL import Image
import os
from os import listdir, getcwd
from os.path import join

trainsets=['set00', 'set01','set02', 'set03', 'set04', 'set05']
testsets=['set06', 'set07','set08', 'set09', 'set10']

classes = ["person", "people", "person?", "person-fa"]

def imagelist(path):
    PNGImages = os.listdir(path)
    nameImages = []
    listImages = []
    for i in range(len(PNGImages)):
       nameImages = PNGImages[i].split(".")
       if nameImages[1] == 'jpg' :
          listImages.append(nameImages[0])

    listImages.sort()
    return listImages

def get_immediate_subdirectories(a_dir):
    return [name for name in sorted(os.listdir(a_dir))
            if os.path.isdir(os.path.join(a_dir, name))]

def convert(size, box):
    dw = 1./(size[0])
    dh = 1./(size[1])
    x = (box[0] + box[1])/2.0 - 1
    y = (box[2] + box[3])/2.0 - 1
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

def convert_annotation(sequence, version, image_id, traintest):
    in_file = open('/home/jugonzal/darknet/data/data-USA/annotations/%s/%s/%s.txt'%(sequence,version, image_id))
    lines = in_file.read().split('\n')[:-1]
    if len(lines) > 1:
        im = Image.open('/home/jugonzal/darknet/data/data-USA/images/%s/%s/%s.jpg'%(sequence,version, image_id))
        out_file = open('/home/jugonzal/darknet/data/data-USA/labels/%s/%s/%s.txt'%(sequence,version, image_id), 'w')
        with open('/home/jugonzal/darknet/data/data-USA/%s.txt'%traintest, "a") as txtfile:
            txtfile.write('/home/jugonzal/darknet/data/data-USA/images/%s/%s/%s.jpg\n'%(sequence,version, image_id))
        w, h = im.size
        person = []
            
        for i in range(1,len(lines)):
            person = lines[i].split(" ")
            #print('../data/data-USA/annotations/%s/%s/%s.txt'%(sequence,version, image_id))
            if len(person) > 4:
               cls_id = classes.index(person[0])
               b = (float(person[1]), float(person[1])  + float(person[3]), float(person[2]), float(person[2]) + float( person[4]))
               bb = convert((w,h), b)
               out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')
        in_file.close()
        out_file.close()

wd = getcwd()

for image_set in trainsets:
    versions = get_immediate_subdirectories('../data/data-USA/images/%s' % image_set)
    for version in versions:
        files = imagelist('../data/data-USA/images/%s/%s'%(image_set, version))
        if not os.path.exists('../data/data-USA/labels/%s/%s'%(image_set, version)):
           os.makedirs('../data/data-USA/labels/%s/%s'%(image_set, version))
        for f in files:
            convert_annotation(image_set,version,f, 'train')

for image_set in testsets:
    versions = get_immediate_subdirectories('../data/data-USA/images/%s' % image_set)
    for version in versions:
        files = imagelist('../data/data-USA/images/%s/%s'%(image_set, version))
        if not os.path.exists('../data/data-USA/labels/%s/%s'%(image_set, version)):
            os.makedirs('../data/data-USA/labels/%s/%s'%(image_set, version))
        for f in files:
            convert_annotation(image_set,version,f, 'test')

