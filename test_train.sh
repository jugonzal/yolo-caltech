#!/bin/bash
#OAR -p gpu='YES' and cluster='dellt630gpu' and dedicated='stars'
#OAR -l/gpunum=4,walltime=36
#OAR --notify mail:juan-diego.gonzales-zuniga@inria.fr

module load cuda/8.0 cudnn/6.0-cuda-8.0
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64"
make -j8
./darknet detector train cfg/caltech.data cfg/yolo-caltech.cfg weights/darknet19_448.conv.23 -gpu 0,1,2,3
